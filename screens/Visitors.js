import React,{useEffect,useState} from 'react';
import { StyleSheet, Text, View ,TextInput ,Image} from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import * as WebBrowser from 'expo-web-browser';
import { RectButton, ScrollView } from 'react-native-gesture-handler';
import { Card, Icon, SearchBar } from 'react-native-elements'
import axios from 'axios'
import {  List, ListItem, Thumbnail, Left, Body, Right ,Button,Header,Item,Input} from 'native-base';
export default function LinksScreen() {
  const [visitors,SetVisitors] = useState([])
  const [Mainvisitors,SetMainVisitors] = useState([])
  const [Search,updateSearch] = useState('')
  const getVisitors = async() => {
    try {
      let {data} = await axios.get('http://172.16.10.28:8977/timebomb/api/v1/users')
      // console.log(data,'sjdfjhadfjn');
      SetVisitors(data)
      SetMainVisitors(data)
    } catch (e) {

    }
  }

  const OnupdateSearch = (query) => {
    console.log(query);
    updateSearch(query)
    console.log(Mainvisitors,"Mainvisitors");
    let searchFiltered = Mainvisitors.filter((r)=>{return r.name.includes(query)})
    if (searchFiltered.length>0) {
      SetVisitors(searchFiltered)
    }else {
      SetVisitors(Mainvisitors)
    }
    console.log('searchFiltered',searchFiltered);
  }
  useEffect(()=>{
    getVisitors()
  },[])
  {console.log(visitors,'visitors')}
  return (

    <ScrollView style={styles.container} contentContainerStyle={styles.contentContainer}>

    <Header searchBar rounded>
          <Item>
            <Icon name="ios-search" />
            <Input placeholder="Search" />
            <Icon name="ios-people" />
          </Item>
          <Button transparent>
            <Text>Search</Text>
          </Button>
        </Header>

      {
        visitors.map((each,index)=>{
          return(
            <List>
           <ListItem avatar>
             <Left>
               <Thumbnail  source={{ uri:'https://cdn3.iconfinder.com/data/icons/avatars-15/64/_Ninja-2-512.png'} }/>
             </Left>
             <Body>
               <Text>{each.name}</Text>
               <Text >Department:{each.department}</Text>
             </Body>
             <Right>
             </Right>
           </ListItem>
         </List>

          )
        })
      }


    </ScrollView>

  );
}

// function OptionButton({ icon, label, onPress, isLastOption }) {
//   return (
//     <RectButton style={[styles.option, isLastOption && styles.lastOption]} onPress={onPress}>
//       <View style={{ flexDirection: 'row' }}>
//         <View style={styles.optionIconContainer}>
//           <Ionicons name={icon} size={22} color="rgba(0,0,0,0.35)" />
//         </View>
//         <View style={styles.optionTextContainer}>
//           <Text style={styles.optionText}>{label}</Text>
//         </View>
//       </View>
//     </RectButton>
//   );
// }

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fafafa',
  },
  contentContainer: {
    paddingTop: 15,
  },
  optionIconContainer: {
    marginRight: 12,
  },
  cardContainer: {
    borderColor: '#e2e2e2'
  },
  option: {
    backgroundColor: '#fdfdfd',
    paddingHorizontal: 15,
    paddingVertical: 15,
    borderWidth: StyleSheet.hairlineWidth,
    borderBottomWidth: 0,
    borderColor: '#ededed',
  },
  lastOption: {
    borderBottomWidth: StyleSheet.hairlineWidth,
  },
  optionText: {
    fontSize: 15,
    alignSelf: 'flex-start',
    marginTop: 1,
  },
  searchbar :{
    margin:1,
    marginTop:20,
    marginBottom:20,
    height:45,
    marginLeft:16,
    borderBottomColor: '#ecf0f1',
    flex:1,


  }
});
