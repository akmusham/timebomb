import React, { Component ,useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  Button,
  TouchableHighlight,
  Image,
  Alert,
  AsyncStorage
} from 'react-native';
import axios from 'axios'
import { NativeRouter, Route,  Redirect } from "react-router-native";
// import {Card} from 'native-base'

// import { NavigationContainer } from '@react-navigation/native';
// import { createStackNavigator } from '@react-navigation/stack';

export default function LoginView({navigation}) {
  const [email,SetEmail] = useState('')
  const [pass,SetPass] = useState('')
  const [err,Seterr] = useState('')
  // const onClickListener = (viewId) => {
  //   navigation.navigate(viewId)
  //   // Alert.alert("Alert", "Button pressed "+viewId);
  // }

  const login = async()=> {
    try {
      let data = {email,pass}
      console.log('data',data);
      let loginRequest = await axios.post('http://172.16.10.28:8977/timebomb/api/v1/authLogin',data)
      Seterr('')
      navigation.navigate('home')
    } catch (e) {
      console.log(e);
      Seterr('Invalid creds!')

    }

  }
    return (
      <View style={styles.container}>
      <Image
        source={require('../assets/images/pwc.png')}
        style={styles.welcomeImage}
      />

        <View style={styles.inputContainer}>
          <Image style={styles.inputIcon} source={{uri: 'https://png.icons8.com/message/ultraviolet/50/3498db'}}/>
          <TextInput style={styles.inputs}
              placeholder="Email"
              keyboardType="email-address"
              underlineColorAndroid='transparent'
              onChangeText={(email) => {SetEmail(email); AsyncStorage.setItem('email',email)}}/>
        </View>

        <View style={styles.inputContainer}>
          <Image style={styles.inputIcon} source={{uri: 'https://png.icons8.com/key-2/ultraviolet/50/3498db'}}/>
          <TextInput style={styles.inputs}
              placeholder="Password"
              secureTextEntry={true}
              underlineColorAndroid='transparent'
              onChangeText={(password) => SetPass(password)}/>
        </View>
        {err !==''?<Text style={{color: 'red',marginTop: -10,marginBottom: 5}}>{err}</Text>:null}


        <TouchableHighlight style={[styles.buttonContainer, styles.loginButton]} onPress={()=> login()}>
          <Text style={styles.loginText}>Login</Text>
        </TouchableHighlight>


        <TouchableHighlight style={styles.buttonContainer}
         onPress={()=> navigation.navigate('register')} >
            <Text style={{color:"#e74c3c"}}>Forgot your password?</Text>
        </TouchableHighlight>


        <TouchableHighlight  onPress={()=> navigation.navigate('register')}  >
            <Text style={{color:"#EB8C00"}}>Register</Text>
        </TouchableHighlight>
      </View>
    );
  }

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
  },
  welcomeImage: {
    width: 100,
    height: 80,
    resizeMode: 'contain',
    marginTop: 0,
    marginLeft: -10,
    marginBottom: 15
  },
  inputContainer: {
      borderBottomColor: '#ecf0f1',
      backgroundColor: '#ecf0f1',
      borderRadius:30,
      borderBottomWidth: 1,
      width:250,
      height:45,
      marginBottom:20,
      flexDirection: 'row',
      alignItems:'center'
  },
  inputs:{
      height:45,
      marginLeft:16,
      borderBottomColor: '#ecf0f1',
      flex:1,

  },
  inputIcon:{
    width:30,
    height:30,
    marginLeft:15,
    justifyContent: 'center'
  },
  buttonContainer: {
    height:45,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom:20,
    width:250,
    borderRadius:30,
  },
  loginButton: {
    backgroundColor: "#DC6900",
  },
  loginText: {
    color: 'white',
  }
});
