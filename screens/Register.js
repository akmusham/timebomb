import React, { Component, useState } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  Button,
  TouchableHighlight,
  Image,
  Alert,
  Picker
} from 'react-native';
import axios  from 'axios'
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

export default function LoginView({navigation}) {
  const [lineOfService,SetLineOfService] = useState({})
  const [name,Setname] = useState('')
  const [email,Setemail] = useState('')
  const [pass,SetPass] = useState('')
  const onClickListener = (viewId) => {
    navigation.navigate(viewId)
    // Alert.alert("Alert", "Button pressed "+viewId);
  }

  const LineOfService  = [
    {label: 'advisory',value: 'advisory'},
    {label: 'risk',value: 'risk'}
  ]

  const Register = async() => {
    console.log('nameemailpass',name,email,pass);
    let data = {
          headers: {
              'Content-Type': 'application/json'
          },
          body : {name,email,pass}

      }
    if (name !==  '' && email !== '' && pass !== '') {
      try {
        let res = await axios.post('http://172.16.10.28:8977/timebomb/api/v1/auth',data)
        navigation.navigate('login')
      } catch (e) {
        console.log(e);
      }

    }
  }

    return (
      <View style={styles.container}>
      <Image
        source={require('../assets/images/pwc.png')}
        style={styles.welcomeImage}
      />
      <View style={styles.inputContainer}>
        <Image style={styles.inputIcon} source={{uri: 'https://png.icons8.com/message/ultraviolet/50/3498db'}}/>
        <TextInput style={styles.inputs}
            placeholder="Name"
            keyboardType="email-address"
            underlineColorAndroid='transparent'
            onChangeText={(name) => Setname(name)}/>
      </View>

        <View style={styles.inputContainer}>
          <Image style={styles.inputIcon} source={{uri: 'https://png.icons8.com/message/ultraviolet/50/3498db'}}/>
          <TextInput style={styles.inputs}
              placeholder="Email"
              keyboardType="email-address"
              underlineColorAndroid='transparent'
              onChangeText={(email) => Setemail(email)}/>
        </View>

        <View style={styles.inputContainer}>
          <Image style={styles.inputIcon} source={{uri: 'https://png.icons8.com/key-2/ultraviolet/50/3498db'}}/>
          <TextInput style={styles.inputs}
              placeholder="Create Password"
              secureTextEntry={true}
              underlineColorAndroid='transparent'
              onChangeText={(password) => SetPass(password)}/>
        </View>


        <TouchableHighlight style={[styles.buttonContainer, styles.loginButton]}
        onPress={() => Register()}>
          <Text style={styles.loginText}>Register</Text>
        </TouchableHighlight>

        <TouchableHighlight style={styles.buttonContainer} onPress={() => navigation.navigate('login')}>
            <Text>Already have account?</Text>

        </TouchableHighlight>
      </View>
    );
  }

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
  },
  welcomeImage: {
    width: 100,
    height: 80,
    resizeMode: 'contain',
    marginTop: 0,
    marginLeft: -10,
    marginBottom: 15
  },
  inputContainer: {
      borderBottomColor: '#ecf0f1',
      backgroundColor: '#ecf0f1',
      borderRadius:30,
      borderBottomWidth: 1,
      width:250,
      height:45,
      marginBottom:20,
      flexDirection: 'row',
      alignItems:'center'
  },
  inputs:{
      height:45,
      marginLeft:16,
      borderBottomColor: '#ecf0f1',
      flex:1,
  },
  inputIcon:{
    width:30,
    height:30,
    marginLeft:15,
    justifyContent: 'center'
  },
  buttonContainer: {
    height:45,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom:20,
    width:250,
    borderRadius:30,
  },
  loginButton: {
    backgroundColor: "#DC6900",
  },
  loginText: {
    color: 'white',
  }
});
