import React,{useState} from 'react';
import { Image, DatePickerIOS, Platform, TextInput, StyleSheet, Text,
TouchableHighlight, TouchableOpacity, View,Icon,Button,AsyncStorage  } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import * as WebBrowser from 'expo-web-browser';
import axios from 'axios'
import {  DatePicker } from 'native-base';
import { MonoText } from '../components/StyledText';
// import DatePicker from 'react-native-date-picker'
// import DateTimePicker from '@react-native-community/datetimepicker';

export default function HomeScreen({navigation}) {
  const [showForm,setshowForm] = useState(false)
  const [Indate, setDate] = useState();
  const [outDate,setOutDate] = useState()
  const [name,Setname] = useState('')
  const [department,Setdepartment] = useState('')
  const [hideSubmit,setHideSubmit] = useState(false)
  // const [chosenDate,setDate] = useState(new Date())
  const SetShowForm = ()=> {
    console.log(showForm);
    setshowForm(prev=>!prev)
  }
  // const SetInTime = (date)=> {
  //   setInTime(date.toISOString())
  // }
  // const setOutTime = async(date)=> {
  //   SetOutTime(date.toISOString())
  //
  // }

  // const onChange = (event, selectedDate) => {
  //   const currentDate = selectedDate || date;
  //
  //   setDate(currentDate);
  //   setShow(Platform.OS === 'ios' ? true : false);
  // };

  const ViewUsers = ()=> {

  }
  const CreateTs  = async() => {
    console.log('name,department,Indate,outDate',name,department,Indate,outDate);
      try {
        let body ={name:name,department:department,Indate: Indate,outDate: outDate}
          if (name !== '' && department !== '' && Indate !== undefined && outDate !== undefined) {
            let res = await axios.post('http://172.16.10.28:8977/timebomb/api/v1/addTs',body)
            setHideSubmit(true)
          }else {
            console.log('fill all fields');
          }
        console.log('break');

      } catch (e) {
        console.log(e);
      }
  }

  return (
        <View style={styles.container} >
          <View style={styles.welcomeContainer}>
              <Image
                source={require('../assets/images/pwc.png')}
                style={styles.welcomeImage}
              />
              <Text style={styles.HeaderText}>Vistors In</Text>

              <View style={styles.inputContainer1}>
                <Image style={styles.inputIcon} source={{uri: 'https://png.icons8.com/key-2/ultraviolet/50/3498db'}}/>
                <TextInput style={styles.inputs}
                  placeholder="name"
                  keyboardType="email-address"
                  underlineColorAndroid='transparent'
                  onChangeText={(name) => Setname(name)}/>
              </View>
                  <View style={styles.inputContainer}>
                    <Image style={styles.inputIcon} source={{uri: 'https://png.icons8.com/key-2/ultraviolet/50/3498db'}}/>
                    <TextInput style={styles.inputs}
                        placeholder="department"
                          keyboardType="email-address"
                        underlineColorAndroid='transparent'
                        onChangeText={(department) => Setdepartment(department)}/>
                </View>
                <View style={styles.inputContainer}>
                <DatePicker
              defaultDate={new Date()}
              minimumDate={new Date(2020, 10, 1)}
              maximumDate={new Date(3020, 12, 31)}
              locale={"en"}
              timeZoneOffsetInMinutes={undefined}
              modalTransparent={false}
              animationType={"fade"}
              androidMode={"default"}
              mode='time'
              placeHolderText="Select intime"
              textStyle={{ color: "green",marginLeft:50 }}
              placeHolderTextStyle={{ color: "#d3d3d3" ,marginLeft:50 }}
              onDateChange={setDate}
              disabled={false}
              />
              </View>
              <View style={styles.inputContainer}>
              <DatePicker
            defaultDate={new Date()}
            minimumDate={new Date(2020, 10, 1)}
            maximumDate={new Date(3020, 12, 31)}
            locale={"en"}
            timeZoneOffsetInMinutes={undefined}
            modalTransparent={false}
            animationType={"fade"}
            androidMode={"default"}
            placeHolderText="Select outime"
            textStyle={{ color: "green",marginLeft:50 }}
            placeHolderTextStyle={{ color: "#d3d3d3" ,marginLeft:50 }}
            onDateChange={setOutDate}
            disabled={false}
            />
            </View>

              {/*<View style={styles.TimeContainer}>
                  {
                    showForm?InTime !== undefined? <Text>{InTime.split('T')[1]}</Text>:<Text>Please login youve so much work do</Text>:null
                  }
                  {
                    showForm?OutTime !== undefined? <Text>{OutTime.split('T')[1]}</Text>:null:null
                  }
              </View>*/}
              <TouchableHighlight onPress={()=> CreateTs()} style={[styles.buttonContainer, styles.loginButton]}>
                <Text style={styles.loginText}>Check-In</Text>
              </TouchableHighlight>

              <TouchableHighlight onPress={()=> navigation.navigate('visitors')} style={[styles.buttonContainer, styles.loginButton]}>
                <Text style={styles.loginText}>View Users</Text>
              </TouchableHighlight>


          </View>


      {/*<View style={styles.tabBarInfoContainer}>
        <Text style={styles.tabBarInfoText}>This is a tab bar. You can edit it in:</Text>

        <View style={[styles.codeHighlightContainer, styles.navigationFilename]}>
          <MonoText style={styles.codeHighlightText}>navigation/BottomTabNavigator.js</MonoText>
        </View>
      </View>*/}
    </View>
  );
}

HomeScreen.navigationOptions = {
  header: null,
};

function DevelopmentModeNotice() {
  if (__DEV__) {
    const learnMoreButton = (
      <Text onPress={handleLearnMorePress} style={styles.helpLinkText}>
        Learn more
      </Text>
    );

    return (
      <Text style={styles.developmentModeText}>
        Development mode is enabled: your app will be slower but you can use useful development
        tools. {learnMoreButton}
      </Text>
    );
  } else {
    return (
      <Text style={styles.developmentModeText}>
        You are not in development mode: your app will run at full speed.
      </Text>
    );
  }
}

function handleLearnMorePress() {
  WebBrowser.openBrowserAsync('https://docs.expo.io/versions/latest/workflow/development-mode/');
}

function handleHelpPress() {
  WebBrowser.openBrowserAsync(
    'https://docs.expo.io/versions/latest/get-started/create-a-new-app/#making-your-first-change'
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ccc',
    flexDirection:"column",
    alignItems:"center",
    justifyContent:"center",
  },
  FloatingButtonStyle: {
    resizeMode: 'contain',
    flex: 1,
    width: 50,
    height: 50,
    // position: 'absolute',
    bottom:80
    //backgroundColor:'black'
  },

  developmentModeText: {
    marginBottom: 20,
    color: 'rgba(0,0,0,0.4)',
    fontSize: 14,
    lineHeight: 19,
    textAlign: 'center',
  },
  HeaderText:{
    marginTop: 10,
    fontSize: 20,
    textAlign: 'center',
  },

  contentContainer: {
    paddingTop: 30,
  },
  inoutContainer:{
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  inputContainer: {
      borderBottomColor: '#F5FCFF',
      backgroundColor: '#FFFFFF',
      borderRadius:30,
      borderBottomWidth: 1,
      width:250,
      height:45,
      marginBottom:20,
      flexDirection: 'row',
      alignItems:'center'
  },
  inputContainer1: {
    marginTop: 50,
      borderBottomColor: '#F5FCFF',
      backgroundColor: '#fff',
      borderRadius:30,
      borderBottomWidth: 1,
      width:250,
      height:45,
      marginBottom:20,
      flexDirection: 'row',
      alignItems:'center'
  },
  inputs:{
      height:45,
      marginLeft:16,
      borderBottomColor: '#ecf0f1',
      flex:1,
  },
  inputIcon:{
    width:30,
    height:30,
    marginLeft:15,
    justifyContent: 'center'
  },
  TimeContainer: {
    flexDirection: 'row',
    // alignItems: 'center',
    justifyContent: 'space-between'
  },
  welcomeContainer: {
    alignItems: 'center',
    marginTop: 60,
    // marginBottom: 20,
  },
  welcomeImage: {
    width: 100,
    height: 80,
    resizeMode: 'contain',
    marginTop: 3,
    marginLeft: -10,
  },
  getStartedContainer: {
    alignItems: 'center',
    marginHorizontal: 50,
  },
  homeScreenFilename: {
    marginVertical: 7,
  },
  codeHighlightText: {
    color: 'rgba(96,100,109, 0.8)',
  },
  codeHighlightContainer: {
    backgroundColor: 'rgba(0,0,0,0.05)',
    borderRadius: 3,
    paddingHorizontal: 4,
  },
  getStartedText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    lineHeight: 24,
    textAlign: 'center',
  },
  tabBarInfoContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: { width: 0, height: -3 },
        shadowOpacity: 0.1,
        shadowRadius: 3,
      },
      android: {
        elevation: 0,
      },
    }),
    alignItems: 'center',
    backgroundColor: '#fbfbfb',
    paddingVertical: 20,
  },
  tabBarInfoText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    textAlign: 'center',
  },
  navigationFilename: {
    marginTop: 5,
  },
  helpContainer: {
    // marginTop: 15,
    alignItems: 'center',
  },
  helpLink: {
    paddingVertical: 15,
  },
  helpLinkText: {
    fontSize: 14,
    color: '#2e78b7',
  },
  buttonContainer: {
    height:45,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom:20,
    width:250,
    borderRadius:30,
  },
  loginButton: {
    backgroundColor: "#AD1B02",
  },
  loginText: {
    color: 'white',
  }
});
